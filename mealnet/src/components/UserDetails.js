import React, {Component} from 'react'
import FoodStore from '../stores/FoodStore'
import Food from './Food'
import FoodForm from './FoodForm'

class UserDetails extends Component{
    constructor(props){
        super(props)
        this.state = {
          foods : []
        }
        this.foodStore = new FoodStore()
        this.add = (food) => {
          this.foodStore.addFood(this.props.item.id, food)
        }
        this.delete = (foodId) => {
          this.foodStore.deleteFood(this.props.item.id, foodId)
        }
        this.save = (foodId, food) => {
          this.foodStore.saveFood(this.props.item.id, foodId, food)
        }
    }
    componentDidMount(){
        this.foodStore.getFoods(this.props.item.id)
        this.foodStore.emitter.addListener('GET_FOOD_SUCCESS', () => {
          this.setState({
            foods : this.foodStore.foods
          })
        })
    }
    render(){
        let {item} = this.props
        return <div>
            I insert food for {item.first_name} {item.last_name}
            <div>
                {
                  this.state.foods.map((e, i) => <Food key={i} item={e} onDelete={this.delete} onSave={this.save} />)
                }
                <FoodForm onAdd={this.add} />
            </div>
            
            <div>
                <input type="button" value="back" onClick={() => this.props.onCancel()} />
            </div>
        </div>
    }
}

export default UserDetails