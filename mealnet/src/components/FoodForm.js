import React, { Component } from 'react'

class FoodForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            category: '',
            expiry_date: '',
            shareable: '',
        }
        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name]: evt.target.value
            })
        }
        this.add = () => {
            
        }
    }
    render() {
        return <div>
            <input type="text" placeholder="name" name="name" onChange={this.handleChange} />
            <input type="text" placeholder="category" name="category" onChange={this.handleChange} /> 
            <input type="text" placeholder="expiry_date" name="expiry_date" onChange={this.handleChange} /> 

            <input type="button" value="add" onClick={this.add} />
        </div>
    }
}

export default FoodForm
