import {EventEmitter} from 'fbemitter'

const SERVER = 'http://3.18.108.71:8080'


class UserStore {
    constructor(){
        this.users = []
        this.currentUser = null;
        this.emitter = new EventEmitter()
    }

    async getUser(id){
        try{
            let response = await fetch(`${SERVER}/users/${id}`)
            let data = await response.json()
            this.currentUser = data
            console.log("current user", this.currentUser);
            this.emitter.emit('GET_USER_SUCCESS')
        }
        catch(err){
            console.warn(err)
            this.emitter.emit('GET_USER_ERROR')
        }
    }

    async register(data){
        try{
            let response = await fetch(`${SERVER}/users/register`, {
                method : 'post',
                headers : {
                    'Content-Type' : 'application/json'
                },
                body : JSON.stringify(data) 
            })
            let user = await response.json()
            console.log('registered', user.userId);
            this.getUser(user.userId);
        }
        catch(err){
            console.warn(err)
            this.emitter.emit('USER_REGISTER_ERROR')
        }
    }
    
    async login(data){
        try{
            let response = await fetch(`${SERVER}/users/login`, {
                method : 'post',
                headers : {
                    'Content-Type' : 'application/json'
                },
                body : JSON.stringify(data)           
            })
            let user = await response.json()
            console.log('registered', user.userId);
            this.getUser(user.userId);
        }
        catch(err){
            console.warn(err)
            this.emitter.emit('USER_LOGIN_ERROR')
        }
    }
    

}

export default UserStore