import React,{Component} from 'react';
import UserStore from '../stores/UserStore'
import UserForm from '../components/UserForm'
import User from '../components/User'
import UserDetails from './UserDetails'

class UserList extends Component{
  constructor(){
    super()
    this.state = {
      users : [],
      selectedUser : null
    }
    this.userStore = new UserStore()
    this.add = (user) => {
      this.userStore.addUser(user)
    }
    this.delete = (id) => {
      this.userStore.deleteUser(id)
    }
    this.save = (id, user) => {
      this.userStore.saveUser(id, user)
    }
    this.select = (user) => {
      this.setState({
        selectedUser : user
      })
    }
    this.cancel = () => {
      this.setState({
        selectedUser : null
      })
    }
  }
  componentDidMount(){
    this.userStore.getUsers()
    this.userStore.emitter.addListener('GET_USERS_SUCCESS',() => {
      this.setState({
        users : this.userStore.users
      })
    })
  }
  render(){
    if (this.state.selectedUser){
      return <UserDetails item={this.state.selectedUser} onCancel={this.cancel} />
    }
    else{
      return <div>
        <h3>A list of users</h3>
        {
          this.state.users.map((e, i) => <User key={i} item={e} onDelete={this.delete} onSave={this.save} onSelect={this.select} />)
        }
        <UserForm onAdd={this.add} />
      </div>      
    }
  }
}

export default UserList
