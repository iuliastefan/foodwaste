import React, { Component } from 'react'
import './register.css';
import Sidebar from './Sidebar.js';
import Facebook from './Facebook.js'
import UserStore from './UserStore.js';
import UserProfile from './UserProfile.js'

class UserForm extends Component {
    constructor(props) {
        super(props)
        this.userStore = new UserStore()
        this.routeChange = this.routeChange.bind(this);
        this.store = new UserStore();
        this.state = {
            first_name: '',
            last_name: '',
            username: '',
            mail: '',
            password: '',
            password2: '',
            food_specifications: ''
        }
        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name]: evt.target.value
            })
        }
        this.handleClick = (evt) => {
            evt.preventDefault();
            this.store.register(this.state);
            console.log(this.store.currentUser)
            window.location.href = `/userProfile`;
        }
        // this.currentUser;
    }
    
    routeChange(newPath) {
        let path = `/newPath`;
        this.props.history.push(path);
    }
    
    render() {
        if(this.state.currentUser){
           return <UserProfile item={this.state.currentUser}
            onCancel={this.cancel}/>
        }
        else {
           
        return ( 
            <div>
                <Sidebar/>
                <div id="login-box">
                    <div className="left">
                        <h1>Sign up</h1>
                    
                        <input type="text" name="username" placeholder="Username" onChange={this.handleChange}/>
                        <input type="text" name="first_name" placeholder="First Name" onChange={this.handleChange}/>
                        <input type="text" name="last_name" placeholder="Last Name" onChange={this.handleChange}/>
                        <input type="text" name="mail" placeholder="E-mail" onChange={this.handleChange}/>
                        <input type="password" name="password" placeholder="Password" onChange={this.handleChange}/>
                        <input type="password" name="password2" placeholder="Retype password" onChange={this.handleChange}/>
                    
                        
                    </div>
                
                    <div className="right">
                        <span className="loginwith">Sign in with<br />social network</span>
                        <button name="signup_submit" className="submit" onClick={this.handleClick}>Sign me up</button>
                        <Facebook className="social-signin facebook"/>
                        <button className="social-signin instagram">Log in with instagram</button>
                    </div>
                    
                    <div className="or">OR</div>
                </div>
            </div>);
        }
    }
}

export default UserForm
