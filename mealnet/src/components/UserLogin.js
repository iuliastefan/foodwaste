import React, { Component } from 'react'
import './login.css';
import Sidebar from './Sidebar.js';

import UserStore from './UserStore.js';



class UserLogin extends Component {
    constructor(props) {
        super(props)
        this.store = new UserStore();
        
        this.userStore = new UserStore()
        this.state = {
            username: '',
            password: '',
        }
        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name]: evt.target.value
            })
        }
        this.handleClick = (evt) => {
            evt.preventDefault();
            console.log('state login', this.state);
            this.userStore.login(this.state);
            console.log(this.store.currentUser);
            // window.location.href = `/userProfile`;

        }
    }
    render() {
        return (
            <div>
                <div className ="headd">
                    <Sidebar/> 
                </div>
           
                <div className ="content">
                    <div id="container">       
                            <form>
                                <label htmlFor="username">Username:</label>
                                <input type="text" id="username" name="username" onChange={this.handleChange}/>
                                <label htmlFor="password">Password:</label>
                                <input type="password" id="password" name="password" onChange={this.handleChange}/>
                                <div id="lower">
                                    <input type="checkbox"/><label htmlFor="checkbox">Remember me</label>
                                    <div id="button">
                                    <button className="login" onClick={this.handleClick}>Login</button>
                                    </div>
                                </div>
                            </form>
                    </div>
                </div>
            </div>
            );
        }
    }

export default UserLogin
