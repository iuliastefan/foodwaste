import React, { Component } from 'react'
import './profile.css';
// import history from "../services/history";
import UserStore from './UserStore'
import FoodForm from './FoodForm'
import FoodStore from '../stores/FoodStore'
import JsonList from './JsonList'
import JsonData from './buyItems.json'

class UserProfile extends Component {
    constructor(props) {
        super(props)
    
        this.userStore = new UserStore();
        this.currentUser = this.props.item;
        
        this.foodStore = new FoodStore();
        
        this.state = {
            name: '',
            category: '',
            expiry_date: ''
        }
        
        this.handleChangeFood = (evt) => {
            this.setState({
                [evt.target.name]: evt.target.value
            })
        }
        this.handleClickFood = (evt) => {
            evt.preventDefault();
            //this.foodStore.addFood(this.currentUser.id, this.food);
            console.log(this.currentUser);
            console.log(this.state)
        }
        var array = [];
        JsonData.map((postDetail, index)=>{
            var fname = postDetail.name
            var fdate = postDetail.date
            var f = new Food(fname, fdate);
            array.push(f);
        })
  
        this.state = {
            buyItems: ["bread"],
            message: '',
            friends: [],
            sharedfood: ["apples"]
        }
        
    }
    
    addF(newItem){
        this.setState({buyItems: [...this.state.buyItems, newItem]})
    }
    
    addFriend(e){
        e.preventDefault();
        const {friends} = this.state;
        const newFriend = this.newFriend.value;

            newFriend !== '' && this.setState({
            friends: [...this.state.friends, newFriend],
            })
        
        this.addFriendForm.reset();
    }
    
    addItem(e){
        const {buyItems} = this.state;
        const newItem = this.newItem.value;
        
        const isOnTheList = buyItems.includes(newItem);
        
        if(isOnTheList){
             this.setState({
            message: 'Is on the list'
        })
        } else {
            newItem !== '' && this.setState({
            buyItems: [...this.state.buyItems, newItem],
            message: ''
        })
        }
        this.addForm.reset();
    }
    removeItem(item){
        const newBuyItems = this.state.buyItems.filter(buyItem => {
            return buyItem !== item
        })
        this.setState({
            buyItems: [...newBuyItems]
        })
        
        const {sharedfood} = this.state;
        const newItem = this.newItem.value;
        item !== '' && this.setState({
            sharedfood: [...this.state.sharedfood, item] })
    }
    claimItem(item){
        const newShare = this.state.sharedfood.filter(shared => {
            return shared !== item
        })
        this.setState({
            sharedfood: [...newShare]
        })
        
       
        item !== '' && this.setState({
            buyItems: [...this.state.buyItems, item] })
    }
    render() {
        const {buyItems, message, friends, sharedfood} = this.state;
        return <div>
            <div class ="head">
            <div class="head-left">
                <div id="logo"  ></div>
            </div>
            <div class="head-right">
                <input class="input_box" type="search" placeholder="Search" />
                <div class="btn">
                    <a href='#'>
                        <i class="fa fa-search"></i>
                    </a>
                </div>
                
            </div>   
        </div>
        <div class ="content">
            <div class="content-left">
           
                <div class="cl">
                    <button type="button">Home</button>
                </div>
                <div class="cl">
                    <button type="button">Inventory</button>
                </div>
                 <div class="cl">
                    
                </div>
                 <div class="cl">
                    <table className="table">
                    <thead>
                    <tr>
                        <th>Friends</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        friends.map(item => {
                            return <tr key={item}>
                            <th>{item}</th>
                             </tr>
                        })
                    }
                        
                    </tbody>
                </table>
                <form ref={(input) => {this.addFriendForm = input}} onSubmit={(e) => {this.addFriend(e)}}>
                     <div className="form-group">
                        <input ref={input => this.newFriend = input}  type="text" placeholder="name" name="name"  />
                        <button type="submit" >Add</button>
                     </div>
                </form>
                    
                </div>
                
                
               
            </div>
            <div class="content-middle">
                <div class="cover"></div>
                <div class="coverr"><img src="/foodwaste/mealnet/src/components/images/fff.jpg" /></div>
                <div class="profilepicc"><img src="/foodwaste/mealnet/public/logo192.png"/ ></div>
                 <table className="table">
                    <thead>
                    <tr>
                        <th>All the shared food</th>
                        <th>Claim</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        sharedfood.map(item => {
                            return <tr key={item}>
                            <th>{item}</th>
                            <th>
                                <button onClick={(e) => this.claimItem(item)}>
                                    Claim
                                </button>
                            </th>
                             </tr>
                        })
                    }
                        
                    </tbody>
                </table>
                <div class="username"></div>
                
                <table className="table">
                    <thead>
                    <tr>
                        <th>My food</th>
                        <th> Share </th>
                    </tr>
                    </thead>
                    <tbody>
                    
                    {
                    
                        buyItems.map(item => {
                            return <tr key={item}>
                            <th>{item}</th>
                            
                            <th>
                                <button onClick={(e) => this.removeItem(item)}>
                                    Share
                                </button>
                            </th>
                             </tr>
                        })
                    }
                        
                    </tbody>
                </table>
                {
                    message !== '' && <p > {message} </p>
                }
                <form ref={(input) => {this.addForm = input}} onSubmit={(e) => {this.addItem(e)}}>
                     <div className="form-group">
                        <input ref={input => this.newItem = input}  type="text" placeholder="name" name="name"  />
                        <input id="foodcategory" type="text" placeholder="category" name="category"  onChange={this.handleChangeFood}/> 
                        <input id="expiry_date" type="text" placeholder="expiry_date" name="expiry_date"  onChange={this.handleChangeFood}/> 

                        <button type="submit" >Add</button>
                     </div>
                </form>
            </div>
            <div class="content-right">
             
             
            <div id="notifs">Notifications</div>
                   
            </div>
           
            </div>
        </div>
    }
}

export default UserProfile

export class Food {
    constructor(name, date) {
        this.name = name;
        this.date = date;
    }
}