import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Sidebar extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="topnav">
                <Link to="/userProfile/:id">Home</Link>
                <Link to="/news">News</Link>
                <Link to="/contact">Contact</Link>
                <Link to="/register">Register</Link>
                <Link to="/login">Login</Link>
            </div>
        );
    }
}

export default Sidebar;