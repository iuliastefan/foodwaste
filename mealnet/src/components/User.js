import React, { Component } from 'react'

class User extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isEditing: false,
            first_name: this.props.item.first_name,
            last_name: this.props.item.last_name,
            mail: this.props.item.mail,
            password: this.props.item.password,
            food_specifications: this.props.item.food_specifications
        }
        this.delete = () => {
            this.props.onDelete(this.props.item.id)
        }
        this.save = () => {
            this.props.onSave(this.props.item.id, {
                first_name: this.state.first_name,
                last_name: this.state.last_name,
                mail: this.state.mail,
                password: this.state.password,
                food_specifications: this.state.food_specifications
            })
            this.setState({
                isEditing: false
            })
        }
        this.edit = () => {
            this.setState({ isEditing: true })
        }
        this.cancel = () => {
            this.setState({ isEditing: false })
        }
        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name]: evt.target.value
            })
        }
        this.select = () => {
            this.props.onSelect(this.props.item)
        }
    }
    render() {
        let { item } = this.props
        if (this.state.isEditing) {
            return <div>
                <h4>
                    <input type="text" name="first_name" onChange={this.handleChange} value={this.state.first_name} />
                    <input type="text" name="last_name" onChange={this.handleChange} value={this.state.last_name} />
                </h4>
                <h6>with mail 
                    <input type="text" name="mail" onChange={this.handleChange} value={this.state.mail} />
                    , food_specifications 
                    <input type="text" name="food_specifications" onChange={this.handleChange} value={this.state.food_specifications} />
                    and password <input type="text" name="password" onChange={this.handleChange} value={this.state.password} />

                </h6>
                <div>
                    <input type="button" value="cancel" onClick={this.cancel} />
                    <input type="button" value="save" onClick={this.save} />
                </div>
            </div>
        }
        else {
            return <div>
                <h4>{item.first_name} {item.last_name}</h4>
                <h6>with mail {item.mail} and food specifications {item.food_specifications}</h6>
                <div>
                    <input type="button" value="delete" onClick={this.delete} />
                    <input type="button" value="edit" onClick={this.edit} />
                    <input type="button" value="select" onClick={this.select} />
                </div>
            </div>
        }

    }
}

export default User
