import React from 'react';
//import { Switch, Route } from 'react-router-dom';
import { Switch } from 'react-router-dom';
import Route from './Route';
import Login from '../components/UserLogin';
import Register from '../components/UserForm';
import UserProfile from '../components/UserProfile';

export default function Routes() {
    return (
        <Switch>
      <Route path="/login" exact component={Login} />
      <Route path="/register" component={Register} />
      <Route path="/userProfile" component={UserProfile} />
      {/* redirect user to Login page if route does not exist and user is not authenticated */}
      <Route component={Login} />
    </Switch>
    );
}
