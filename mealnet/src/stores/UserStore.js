import { EventEmitter } from 'fbemitter'

const SERVER = "http://3.135.193.251:8080"

class UserStore {
    constructor() {
        this.users = [];
        this.emitter = new EventEmitter()
    }

    async getUsers() {
        try {
            let response = await fetch(`${SERVER}/users`)
            let data = await response.json()
            this.users = data;
            this.emitter.emit('GET_USERS_SUCCESS')

        }
        catch (err) {
            console.warn(err);
            this.emitter.emit('GET_USERS_ERROR')
        }
    }

    async addUser(user) {
        try {
            await fetch(`${SERVER}/users/register`, {
                method: 'post',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(user)
            })
            this.getUsers()
        }
        catch (err) {
            console.warn(err)
            this.emitter.emit('ADD_USERS_ERROR')
        }
    }

    async deleteUser(id){
        try {
            await fetch(`${SERVER}/users/${id}`, {
                method : 'delete'
            })
            this.getUsers()
        } catch (err) {
            console.warn(err)
            this.emitter.emit('DELETE_USERS_ERROR')
        }
    }
    async saveUser(id, user) {
        try {
            await fetch(`${SERVER}/users/${id}`, {
                method: 'put',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(user)
            })
            this.getUsers()
        }
        catch (err) {
            console.warn(err)
            this.emitter.emit('UPDATE_USERS_ERROR')
        }

    }
}

export default UserStore
