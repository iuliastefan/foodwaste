import { EventEmitter } from 'fbemitter'

const SERVER = "http://3.135.193.251:8080"

class FriendStore {
    constructor() {
        this.friends = [];
        this.emitter = new EventEmitter()
    }

    async getFriends(userId) {
        try {
            let response = await fetch(`${SERVER}/users/${userId}/friends`)
            let data = await response.json()
            this.friends = data;
            this.emitter.emit('GET_FRIENDS_SUCCESS')
        }
        catch (err) {
            console.warn(err);
            this.emitter.emit('GET_FRIENDS_ERROR')
        }
    }

    async addFriend(userId, friend) {
        try {
            await fetch(`${SERVER}/users/${userId}/friends`, {
                method: 'post',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(friend)
            })
            this.getFriends(userId)
        }
        catch (err) {
            console.warn(err)
            this.emitter.emit('ADD_FRIEND_ERROR')
        }
    }

    async deleteFriend(userId, friendId) {
        try {
            await fetch(`${SERVER}/users/${userId}/friends/${friendId}`, {
                method: 'delete',
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            this.getFriends(userId)
        }
        catch (err) {
            console.warn(err)
            this.emitter.emit('DELETE_FRIEND_ERROR')
        }

    }
    async editFriend(userId, friendId, friend) {
        try {
            await fetch(`${SERVER}/users/${userId}/friends/${friendId}`, {
                method: 'put',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(friend)
            })
            this.getFriends(userId)
        }
        catch (err) {
            console.warn(err)
            this.emitter.emit('UPDATE_FRIEND_ERROR')
        }
    }
}

export default FriendStore
