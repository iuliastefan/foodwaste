import { EventEmitter } from 'fbemitter'

const SERVER = "http://3.135.193.251:8080"

class FoodStore {
    constructor() {
        this.foods = [];
        this.emitter = new EventEmitter()
    }

    async getFoods(userId) {
        try {
            let response = await fetch(`${SERVER}/users/${userId}/foods`)
            let data = await response.json()
            this.foods = data;
            this.emitter.emit('GET_FOODS_SUCCESS')
        }
        catch (err) {
            console.warn(err);
            this.emitter.emit('GET_FOODS_ERROR')
        }
    }

    async addFood(userId, food) {
        try {
            await fetch(`${SERVER}/users/foods`, {
                method: 'post',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(food)
            })
            this.getFoods(userId)
        }
        catch (err) {
            console.warn(err)
            this.emitter.emit('ADD_FOOD_ERROR')
        }
    }

    async deleteFood(userId, foodId) {
        try {
            await fetch(`${SERVER}/users/${userId}/foods/${foodId}`, {
                method: 'delete',
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            this.getFoods(userId)
        }
        catch (err) {
            console.warn(err)
            this.emitter.emit('DELETE_FOOD_ERROR')
        }

    }
    async editFood(userId, foodId, food) {
        try {
            await fetch(`${SERVER}/users/${userId}/foods/${foodId}`, {
                method: 'put',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(food)
            })
            this.getFoods(userId)
        }
        catch (err) {
            console.warn(err)
            this.emitter.emit('UPDATE_FOOD_ERROR')
        }
    }
}

export default FoodStore
