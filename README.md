# FoodWaste

Roles:
1. Frontend: Diana Zepisi, Cristian Vasile
2. Backend: Iulia Stefan, Delia Varzariu
3. Database: Daniela Vlad

Functionalities:
The application will have multiple users. Each user can define a group of friends and label them, depending on their food preferences. A user can define custom labels for their friends. 
Each user will have a list of products that they have in their fridge, pantry etc. Each product shall have a name, quantity and an expiration date. By default, a product will be labeled as unshareable. 
The user will get a notification when the product nears its expiration date and they can decide if they want to share it with friends. 
Users can check the shareable items lists of their friends and claim products. The original owner can either accept or decline the claim. 
The app will integrate social media platforms.

Added basic functionalities in backend with persitence. Database is a work in progress. 

Installation:
1. clone the project in aws cloud9
2. run in the orm and mealnet directorie the commands
npm install
npm start