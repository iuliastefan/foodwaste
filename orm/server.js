const express = require('express')
const bodyParser = require('body-parser')
const Sequelize = require('sequelize')
const Op = Sequelize.Op


const app = express()
app.use(bodyParser.json())

app.use(function(req,res,next){
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
    res.header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT, DELETE')
    next()
})


const sequelize = new Sequelize('project_db', 'app', 'welcome123', {
    dialect: 'mysql'
})

// sequelize.authenticate()
// 	.then(() => console.log('we are connected'))
// 	.catch((error) => console.log(error))


let User = sequelize.define('user', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        unique: true,
        allowNull: false,
        validate: {
            min: 0,
            max: 999
        }
    },

    first_name: {
        type: Sequelize.STRING,
        allowNull: false,
        validate: {
            len: [2 - 20]
        }
    },

    last_name: {
        type: Sequelize.STRING,
        allowNull: false,
        validate: {
            len: [2 - 20]
        }
    },

    mail: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
        validate: {
            isEmail: true
        },
    },

    password: {
        type: Sequelize.STRING,
        allowNull: false,
        validate: {
            len: [7 - 15]
        }
    },

    food_specifications: {
        type: Sequelize.STRING,
        allowNull: true,
        validate: {
            len: [2 - 20]
        }
    },

    // friends: {
        
    // }
})

let Friend = sequelize.define('friend', {
    id: {
        primaryKey: true,
        unique: true,
        type: Sequelize.INTEGER,
        allowNull: false,
        validate: {
            len: [1 - 5]
        }
    },

    name: {
        type: Sequelize.STRING,
        allowNull: false,
        validate: {
            len: [1 - 50]
        }
    },

})

let Food = sequelize.define('food', {
    // user_id: {
    //     type: Sequelize.INTEGER,
    //     allowNull: false,
    //     validate: {
    //         len: [1 - 2]
    //     }
    // },

    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        allowNull: false,
        validate: {
            len: [1 - 2]
        }
    },

    name: {
        type: Sequelize.STRING,
        allowNull: false,
        validate: {
            len: [6 - 20]
        }
    },

    category: {
        type: Sequelize.STRING,
        allowNull: false,
       
    },

    expiry_date: {
        type: Sequelize.DATEONLY,
        allowNull: false,
        validate: {
            len: [6 - 8]
        }
    },

    shareable: {
        type: Sequelize.BOOLEAN,
        allowNull: false
    }
})

User.hasMany(Friend)
User.hasMany(Food)

app.get('/sync', async(req, res, next) => {
    try {
        await sequelize.sync({ force: true })
        res.status(201).json({ message: 'created' })
    }
    catch (err) {
        next(err)
    }
})

/*
 * Create a new user.
 */

User.sync({ alter: true })
Food.sync({ alter: true })

app.post('/sync', async (req,res, next)=>{
    try{
        await sequelize.sync({force:true})
        res.status(201).json({message:'created'})
    }catch(err){
        next(err)
    }
})

app.post('/users/register', async(req, res, next) => {
    
    //a new user will be assigned a randomly generated id and default name and food preferences
    let user = req.body
    console.log("req body")
    console.log(req.body)
    user.id = Math.floor(Math.random() * 1000);
    
    //validations - passwords must be equal
    if (user.password != user.password2)
        return res.status(400).json({message: "Passwords don't match"});
    
 
    try {
        //validations - no duplicates (search by email)
        let sameUser = await User.findOne({ where: {mail: user.mail} });
        if (sameUser)
            return res.status(400).json({message: "Email address already in use"});
        
        await User.create(user)

        res.status(201).json({ message: 'created', userId: user.id })
        
    }
    catch (err) {
        next(err)
    }
})

/*
 * Log in
 */
app.post('/users/login', async(req, res, next) => {

        let user = await User.findOne({
            where: {
                username: req.body.username,
                password: req.body.password
            }
        })
        if (!user)
            res.status(404).json({ message: 'Invalid email or password' })
        else {
            res.status(201).json({ message: 'Welcome back!', userId: user.id })
        }
    
})


/* 
 * Get a specific user
 */
app.get('/users/:id', async(req, res, next) => {
    try {
        let user = await User.findByPk(req.params.id)
        if (user) {
            res.status(200).json(user)
        }
        else {
            res.status(404).json({ message: 'user not found' })
        }
    }
    catch (err) {
        next(err)
    }
})

app.get('/users', async(req, res, next) => {
    try {
        let filter = req.query.filter ? req.query.filter :''
        let pageSize = req.query.pageSize ? parseInt(req.query.pageSize) : 10
        let page = req.query.page ? parseInt(req.query.page) : 0
        let users
        if (page || filter) {
            users = await User.findAll({
                where: {
                    mail: {
                        [Op.like]: `%${filter}%`
                    }
                },
                limit: pageSize,
                offset:page * pageSize
            })
        }
        else {
            users = await User.findAll()
        }
        res.status(200).json(users)
    }
    catch (err) {
        next(err)
    }
})

app.put('/users/:id', async(req, res, next) => {
    try {
        let user = await User.findByPk(req.params.id)
        if (user) {
            await user.update(req.body)
            res.status(202).json({ message: 'accepted' })
        }
        else {
            res.status(404).json({ message: 'not found' })
        }
    }
    catch (err) {
        next(err)
    }
})

app.delete('/users/:id', async(req, res, next) => {
    try {
        let user = await User.findByPk(req.params.id)
        if (user) {
            await user.destroy()
            res.status(202).json({ message: 'accepted' })
        }
        else {
            res.status(404).json({ message: 'not found' })
        }
    }
    catch (err) {
        next(err)
    }
})


//get food for specific user
app.get('/users/:uid/foods', async(req, res, next) => {
    try {
        let user = await User.findByPk(req.params.uid, {
            include: [Food]
        })
        if (user) {
            res.status(200).json(user.foods)
        }
        else {
            res.status(404).json({ message: 'not found' })
        }
    }
    catch (err) {
        next(err)
    }
})

//add food for specific user
app.post('/users/foods', async(req, res, next) => {
    try {
        let model = req.body
        model.id = Math.floor(Math.random() * 1000); 
        //let user = await User.findByPk(req.params.uid)
        //if (user) {
            let food = req.body
            // food.userId = user.id
            await Food.create(food)
            res.status(201).json({ message: 'created' })
        // }
        // else {
        //     res.status(404).json({ message: 'not found' })
        // }
    }
    catch (err) {
        next(err)
    }
})

// //get specific food item for specific user
app.get('/users/:uid/foods/:fid', async(req, res, next) => {
    try {
        let user = await User.findByPk(req.params.uid)
        if (user) {
            let foods = await user.getFoods({
                where: {
                    id: req.params.fid
                }
            })
            let food = foods.shift()
            if (food) {
                res.status(200).json(food)
            }
            else {
                res.status(404).json({ message: 'not found' })
            }
        }
        else {
            res.status(404).json({ message: 'not found' })
        }
    }
    catch (err) {
        next(err)
    }
})

//upate  specific food item for specific user
app.put('/users/:uid/foods/:fid', async(req, res, next) => {
    try {
        let user = await User.findByPk(req.params.uid)
        if (user) {
            let foods = await user.getFoods({
                where: {
                    id: req.params.fid
                }
            })
            let food = foods.shift()
            if (food) {
                await food.update(req.body, {
                    fields: ['name', 'category', 'expiry_date', 'shareable']
                })
                res.status(202).json({ message: 'accepted' })
            }
            else {
                res.status(404).json({ message: 'not found' })
            }
        }
        else {
            res.status(404).json({ message: 'not found' })
        }
    }
    catch (err) {
        next(err)
    }
})

// //delete specific food item for specific user
app.delete('/users/:uid/foods/:fid', async(req, res, next) => {
    try {
        let user = await User.findByPk(req.params.uid)
        if (user) {
            let foods = await user.getFoods({
                where: {
                    id: req.params.fid
                }
            })
            let food = foods.shift()
            if (food) {
                await food.destroy()
                res.status(202).json({ message: 'accepted' })
            }
            else {
                res.status(404).json({ message: 'not found' })
            }
        }
        else {
            res.status(404).json({ message: 'not found' })
        }
    }
    catch (err) {
        next(err)
    }
})

//TODO: add friends functionality
//get friend for specific user
app.get('/users/:uid/friends', async(req, res, next) => {
    try {
        let user = await User.findByPk(req.params.uid, {
            include: [Friend]
        })
        if (user) {
            res.status(200).json(user.friends)
        }
        else {
            res.status(404).json({ message: 'not found' })
        }
    }
    catch (err) {
        next(err)
    }
})

//add friend for specific user
app.post('/users/:uid/friends', async(req, res, next) => {
    try {
        let user = await User.findByPk(req.params.uid)
        if (user) {
            let friend = req.body
            friend.userId = user.id
            await Food.create(friend)
            res.status(201).json({ message: 'created' })
        }
        else {
            res.status(404).json({ message: 'not found' })
        }
    }
    catch (err) {
        next(err)
    }
})

// //get specific friend item for specific user
app.get('/users/:uid/friends/:fid', async(req, res, next) => {
    try {
        let user = await User.findByPk(req.params.uid)
        if (user) {
            let friends = await user.getFoods({
                where: {
                    id: req.params.fid
                }
            })
            let friend = friends.shift()
            if (friend) {
                res.status(200).json(friend)
            }
            else {
                res.status(404).json({ message: 'not found' })
            }
        }
        else {
            res.status(404).json({ message: 'not found' })
        }
    }
    catch (err) {
        next(err)
    }
})

//upate  specific friend item for specific user
app.put('/users/:uid/friends/:fid', async(req, res, next) => {
    try {
        let user = await User.findByPk(req.params.uid)
        if (user) {
            let friends = await user.getFriends({
                where: {
                    id: req.params.fid
                }
            })
            let friend = friends.shift()
            if (friend) {
                await friend.update(req.body, {
                    fields: ['name']
                })
                res.status(202).json({ message: 'accepted' })
            }
            else {
                res.status(404).json({ message: 'not found' })
            }
        }
        else {
            res.status(404).json({ message: 'not found' })
        }
    }
    catch (err) {
        next(err)
    }
})

// //delete specific friend item for specific user
app.delete('/users/:uid/friends/:fid', async(req, res, next) => {
    try {
        let user = await User.findByPk(req.params.uid)
        if (user) {
            let friends = await user.getFriends({
                where: {
                    id: req.params.fid
                }
            })
            let friend = friends.shift()
            if (friend) {
                await friend.destroy()
                res.status(202).json({ message: 'accepted' })
            }
            else {
                res.status(404).json({ message: 'not found' })
            }
        }
        else {
            res.status(404).json({ message: 'not found' })
        }
    }
    catch (err) {
        next(err)
    }
})



app.use((err, req, res, next) => {
    console.warn(err)
    res.status(500).json({ message: 'server error :( very sad :(' })
})

const port = 8080
app.listen(port, () => {
    console.log(`Running on http://localhost:${port}`)
})
