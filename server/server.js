const express = require('express')
const bodyParser = require('body-parser')

const app = express()
app.use(bodyParser.json());

// const userRouter = require('./users.js')

app.locals.users = [
    {
        "id": "user1",
        "name": "Benny",
        "email": "benny.bonanza@gmail.com",
        "country": "Romania",
        "city": "Bucharest",
        "occupation": "bartender",
        "friends": [
            {
                "id": "user2",
                "label": "that annoying vegetarian"
            }
        ],
        "foodItems": [
            {
                "name": "milk",
                "expiration date": "12/02/2020"
            },
            {
                "name": "meat",
                "expiration date": "12/12/2023"
            }
        ]
    },
    {
        "id": "user2",
        "name": "Sammy",
        "email": "sammy-salami@yahoo.com",
        "country": "USA",
        "city": "Los Angeles",
        "occupation": "demon hunter",
        "friends": [
            {
                "id": "user1",
                "label": "crueless meat-lover"
            }
        ],
        "foodItems": [
            {
                "name": "milk",
                "expiration date": "12/02/2020"
            },
            {
                "name": "meat",
                "expiration date": "12/12/2023"
            }
        ]
    },
    {
        "id": "user3",
        "name": "Lenny",
        "email": "lennyloo@hotmail.com",
        "country": "Australia",
        "city": "Melbourne",
        "occupation": "cabaret dancer",
        "friends": [],
        "foodItems": []
    }
]

//get a list of all users
app.get('/users', (req, res) => {
    res.status(200).json(app.locals.users)
})

//get a specific user
app.get('/users/:id', (req, res) => {
    const user = app.locals.users.find(user => user.id == req.params.id);
    if (!user)
        res.status(404).json({ message: "user not found" });

    res.status(200).json(user);
})

//get a specific user's list of friends
app.get('/users/:id/friends', (req, res) => {
    const user = app.locals.users.find(user => user.id == req.params.id);
    if (!user)
        res.status(404).json({ message: "user not found" });

    if (!user.friends || user.friends.length == 0)
        res.status(200).json({ message: "user has no friends" });

    let friends = [];
    // for (let id of user.friends) {
    //     const friend = app.locals.users.find(friend => friend.id == id);
    //     friends.push(friend);
    // }
    for (let friend of user.friends) {
        friends.push(friend);
    }
    res.status(200).json({ friends: friends });
})

//register a new user into the application - users without email address should not be able to register
//if the new user has a list of friends, each of their friend's friend list is updated 
app.post('/users', (req, res) => {
    try {
        if (!req.body.email)
            res.status(404).json({ message: "user does not have email" })
        else {
            const user = req.body;
            user.id = "user" + app.locals.users.length + 1;

            if (user.friends && user.friends.length > 0) {
                for (let id of user.friends) {
                    const friend = app.locals.users.find(friend => friend.id == id);
                    friend.friends.push(user.id);
                }
            }

            app.locals.users.push(user);
            res.status(201).json({ message: "attempt successful!" })
        }
    }
    catch (err) {
        res.status(500).json({ message: "attempt failed" })
    }
})

//edit specific user - not fully implemented
app.put('/users/:id', (req, res) => {
    try {
        const user = app.locals.users.find(user => user.id == req.params.id);
        if (!user)
            res.status(404).json({ message: "user not found" })
        else {
            // user = req.body; -- for some reason this causes errors :(
            res.status(201).json({ message: "attempt successful!" })
        }
    }
    catch (err) {
        res.status(500).json({ message: "attempt failed" })
    }
})

//get a specific user's list of food items
app.get('/users/:id/food', (req, res) => {
    const user = app.locals.users.find(user => user.id == req.params.id);
    if (!user)
        res.status(404).json({ message: "user not found" });

    if (!user.friends || user.friends.length == 0)
        res.status(200).json({ message: "user has no food" });

    let foodItems = [];
    for (let item of user.foodItems) {
        foodItems.push(item);
    }
    res.status(200).json({ foodItems: foodItems });
})

app.listen(8080)