const express = require('express')
const router = express.Router()


// router.locals.users = [
//     {
//         "id": 1,
//         "name": "Benny",
//         "country": "Romania",
//         "city": "Bucharest",
//         "occupation": "bartender"
//     }, {
//         "id": 2,
//         "name": "Sammy",
//         "country": "USA",
//         "city": "Los Angeles",
//         "occupation": "demon hunter"
//     }
// ]


router.get('/users', (req, res) => {
	res.status(200).json(app.locals.users)
})

router.post('/users', (req, res) => {
    try {
        app.locals.users.push(req.body);
        res.status(201).json({message: "attempt successful!"})
    }
    catch (err) {
        res.status(500).json({message: "attempt failed"})
    }
})

module.exports = router
